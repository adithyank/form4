
import 'package:flutter/material.dart';
import 'form4widget.dart';

class ValueBinders
{
  static ValueBinder<String> text(TextEditingController controller)
  {
    return ValueBinder(
        getter: () => controller.text,
        setter: (val) => controller.text = val ?? ''
    );
  }
}
