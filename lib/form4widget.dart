import 'package:flutter/material.dart';

class ValueBinder<T>
{
  void Function(T? val) setter;
  T? Function() getter;

  ValueBinder({required this.setter, required this.getter});
}

class ValueTransformer
{
  dynamic Function(dynamic inputValue) sendToBinder;
  dynamic Function(dynamic binderValue) takeFromBinder;

  ValueTransformer({required this.sendToBinder, required this.takeFromBinder});

  static ValueTransformer asis<T>()
  {
    return ValueTransformer(
        sendToBinder: (iv) => iv as T?,
        takeFromBinder: (bv) => bv
    );
  }
}


class Form4FieldConfig
{
  final bool showLabel;
  final bool leftRightLabel;

  ///It is the percentage inside the widthPercent
  final int labelWidthPercent;
  
  ///This defines the overall widget percent in the available row space
  final int widthPercent;

  final bool skipInValueCollection;

  const Form4FieldConfig({
    this.showLabel = true,
    this.leftRightLabel = false,
    this.labelWidthPercent = 30,
    this.widthPercent = 100,
    this.skipInValueCollection = false
  });

  bool get placeLabelAtTop => !leftRightLabel;
}

class Form4Field<T> extends StatefulWidget
{
  final Form4Controller formController;
  final String name;
  final String? label;
  final ValueBinder<T> valueBinder;
  final ValueTransformer? valueTransformer;
  final FormFieldValidator<T>? validator;
  final Object? initialValue;
  final Form4FieldConfig config;
  final Widget Function(Form4FieldState<T> fieldState) builder;

  Form4Field(this.formController, this.name, {
      this.label, super.key,
      this.validator, this.initialValue, this.valueTransformer,
      Form4FieldConfig? config,
      required this.valueBinder, required this.builder
  }):
    config = config ?? const Form4FieldConfig()
  {
    // if (kDebugMode) {
    //   print('Form4Field constructor: $name');
    // }

    this.formController.addField(this);

    var val = valueTransformer == null || initialValue == null
        ? initialValue
        : valueTransformer!.sendToBinder(initialValue);

    valueBinder.setter(val);
  }

  @override
  State<Form4Field<T>> createState() => Form4FieldState<T>();
}

class Form4FieldState<T> extends State<Form4Field<T>> {

  T? get currentValue => widget.valueBinder.getter();

  String get name => widget.name;
  String? get label => widget.label;


  @override
  Widget build(BuildContext context) {
    //print('Form4FieldState: build():   $name: ${widget.initialValue}');

    Widget ff = FormField<T>(
      validator: widget.formController.validator(this),
      builder: (field) {
        return Padding(
          padding: const EdgeInsets.all(0),
          child: InputDecorator(
            decoration: InputDecoration(
              filled: false,
              contentPadding: EdgeInsets.zero,
                errorText: field.errorText,
                //labelText: widget.config.showLabel && widget.config.placeLabelAtTop ? widget.label : null,
                border: InputBorder.none,
                focusedBorder: InputBorder.none,
                enabledBorder: InputBorder.none,
                errorBorder: InputBorder.none,
                disabledBorder: InputBorder.none,
                //filled: true
            ),
            child: widget.builder(this)
          ),
        );
      }
    );

    if (widget.config.showLabel && widget.config.leftRightLabel) {
      ff = Row(
        children: [
          Expanded(flex: widget.config.labelWidthPercent, child: Text(widget.label ?? '')),
          Expanded(flex: 100 - widget.config.labelWidthPercent, child: ff),
        ]
      );
    }
    // else if (widget.config.showLabel && !widget.config.leftRightLabel) { //top down label
    //   ff = Column(
    //     //mainAxisSize: MainAxisSize.min,
    //     children: [
    //       Text(widget.label ?? ''),
    //       const SizedBox(height: 5),
    //       ff
    //     ]
    //   );
    // }

    if (widget.config.widthPercent == 100) {
      return ff;
    }
    else {
      return Row(
        children: [
          Expanded(flex: widget.config.widthPercent, child: ff),
          Expanded(flex: 100 - widget.config.widthPercent, child: Container())
        ]
      );
    }
  }
}

class Form4Controller
{
  GlobalKey<FormState> formKey = GlobalKey();

  Map<String, String> fieldErrors = {};
  Map<String, Form4Field<Object?>> fields = {};

  FormFieldValidator<T> validator<T>(Form4FieldState<T> field)
  {
    return (v) {
      String? res;
      if (field.widget.validator != null) {
        res = field.widget.validator!(field.currentValue);
        if (res != null && res.isNotEmpty) {
          return res;
        }
      }

      return fieldErrors.containsKey(field.widget.name)
          ? fieldErrors[field.widget.name]!
          : null;
    };
  }

  void addField<T>(Form4Field<T> field)
  {
    fields[field.name] = field;
  }

  Map<String, dynamic> currentValuesForNonSkippedFields()
  {
    List<String> requiredFields = fields.values
        .where((f) => !f.config.skipInValueCollection)
        .map((f) => f.name)
        .toList();

    return currentValuesFor(requiredFields);
  }

  Map<String, dynamic> currentValuesForSkippedFields()
  {
    List<String> requiredFields = fields.values
        .where((f) => f.config.skipInValueCollection)
        .map((f) => f.name)
        .toList();

    return currentValuesFor(requiredFields);
  }

  Map<String, Object?> currentValuesFor(List<String> fieldNames)
  {
    Map<String, Object?> ret = {};
    for (var field in fields.values) {
      if (fieldNames.contains(field.name)) {
        var val = field.valueBinder.getter();

        var a;

        if (field.valueTransformer == null) {
          a = val;
        }
        else {
          a = field.valueTransformer!.takeFromBinder(val);
        }

        ret[field.name] = a;

        // if (kDebugMode) {
        //   debugPrint('value: ${field.name} = ${ret[field.name]}');
        // }
      }
    }

    return ret;
  }


  void clearFieldErrors()
  {
    fieldErrors.clear();
  }

  void setFieldErrors(Map<String, String> fieldErrorMap)
  {
    clearFieldErrors();
    fieldErrors.addAll(fieldErrorMap);
  }

  void setFieldError(String fieldName, String errorMsg)
  {
    fieldErrors[fieldName] = errorMsg;
  }

  bool validate()
  {
    return formKey.currentState!.validate();
  }

  void validateAndSave({
    required Future<SaveResult> Function()? saveWork,
    void Function(SaveResult result)? onFailure,
    void Function(SaveResult result)? onSuccess}) async
  {
    fieldErrors.clear();

    bool success = validate();
    if (!success) {
      return;
    }

    if (saveWork == null) {
      return;
    }

    SaveResult result = await saveWork();

    if (result.status == SaveStatus.validationFailed) {
      if (result.hasFieldErrors) {
        fieldErrors.addAll(result.fieldErrors!);
        validate();
      }
    }

    Function(SaveResult)? fn = result.success ? onSuccess : onFailure;
    if (fn != null) {
      fn(result);
    }
  }
}

enum SaveStatus
{
  validationFailed,
  saveFailed,
  saveSuccess,
  otherError
}

class SaveResult
{
  SaveStatus status;
  Map<String, String>? fieldErrors;
  String? msg;

  String? postSaveObjId;
  dynamic postSaveObj;

  SaveResult({required this.status, this.fieldErrors, this.msg, this.postSaveObjId, this.postSaveObj});

  bool get hasFieldErrors => fieldErrors != null && fieldErrors!.isNotEmpty;
  bool get success => status == SaveStatus.saveSuccess;
  bool get failure => !success;
  bool get hasMsg  => msg != null && msg!.isNotEmpty;
}



class Form4Config
{
  final String? title;
  final void Function(SaveResult result)? onFailure;
  final void Function(SaveResult result)? onSuccess;

  const Form4Config({this.title, this.onSuccess, this.onFailure});
}

class Form4 extends StatefulWidget {
  final Form4Controller controller;
  final Widget child;
  final Form4Config config;
  final Future<SaveResult> Function()? saveWork;

  const Form4({required this.controller, required this.child,
    this.config = const Form4Config(),
    this.saveWork, super.key});

  @override
  State<Form4> createState() => _Form4State();
}

class _Form4State extends State<Form4> {

  @override
  Widget build(BuildContext context) {
    // if (kDebugMode) {
    //   print('_Form4State: build()... widget.controller.fields.len: ${widget.controller.fields.length}');
    // }

    return Form(
        key: widget.controller.formKey,
        child: Column(
          children: [
            AppBar(
              title: widget.config.title != null && widget.config.title!.isNotEmpty ? Text(widget.config.title!) : null,
              actions: [
                ElevatedButton(
                    onPressed: () => widget.controller.validateAndSave(
                        saveWork: widget.saveWork,
                        onSuccess: widget.config.onSuccess,
                        onFailure: widget.config.onFailure
                    ),
                    child: const Text('Save')
                )
              ],
            ),
            widget.child
          ],
        )
    );
  }
}
