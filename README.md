<!--
This README describes the package. If you publish this package to pub.dev,
this README's contents appear on the landing page for your package.

For information about how to write a good package README, see the guide for
[writing package pages](https://dart.dev/guides/libraries/writing-package-pages).

For general information about developing packages, see the Dart guide for
[creating packages](https://dart.dev/guides/libraries/create-library-packages)
and the Flutter guide for
[developing packages and plugins](https://flutter.dev/developing-packages).
-->

Simple Customizable Flutter Form that Inherently Supports Server Side
Validation or Two Step Validation.

## Features

Build your super forms using your own widgets. This `Form4` widget will
not ask you to use its own `Widget`s, rather it encourages you to build
your own super looking widgets in the `Form4` widget with a little
`Form4Field` wrapper widget.

It helps connect your own controllers and the concept of `ValueBinder`
used by `Form4` widget. This `ValueBinder` helps to collect all the
field values during Form Submission and helps to set the existing
values while opening the form.

## Getting started

1. Include this dependency in your `pubspec.yaml`
2. Add this line `import 'package:form4/form4.dart';`

## Usage

```dart

@override
Widget build(BuildContext context) {
  return Form4(
      controller: formController,
      child: Column(
          children: [
            Form4Field<String>(formController, 'username',
              builder: (field) => TextField(
                controller: userController,
                decoration: InputDecoration(
                    filled: true,
                    labelText: 'Username',
                    border: InputBorder.none
                ),
              ),
              valueBinder: ValueBinders.text(userController),
              validator: Validators.stringShouldNotBeEmpty('Value Required'),
            ),
            Form4Field<String>(formController, 'password',
              builder: (field) {
                return TextField(
                    controller: pwdController,
                    obscureText: true,
                    decoration: const InputDecoration(
                        labelText: 'Password',
                        filled: true,
                        border: InputBorder.none
                    )
                );
              },
              valueBinder: ValueBinders.text(pwdController),
              validator: Validators.stringShouldNotBeEmpty('Pwd Required'),
            ),
            FilledButton(
                onPressed: () {
                  // DevConsole.printIfDebug('u: ${userController.text}');
                  // DevConsole.printIfDebug('p: ${pwdController.text}');
                  formController.validate();
                },
                child: const Text('Submit')
            )
          ]
      ).onScroll().paddingAll(20)
  );
}

```

