## 1.1.0

1. Support added for Form Save and delegating the server call
2. Obtaining back the results and show the widget wise errors
   accordingly (**This is the very Objective of this package**)
4. Concept of `ValueTransformer`s introduced to ensure the
   re-usability of the value transformation functionality
   before and after the value setting/getting from the
   `Controller`s of the widgets

## 1.0.0

* First version of `Form4` widget that does validation 
    in 2 steps, facilitating injecting of field wise
    errors in the `Form4Controller`
* This helps complex validations that needs to be done
  in the server side (like existence of user by the same
  name given in the `TextField`)
